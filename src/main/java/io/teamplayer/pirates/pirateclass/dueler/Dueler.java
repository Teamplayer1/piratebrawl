package io.teamplayer.pirates.pirateclass.dueler;

import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Dueler extends PirateClass {

    public Dueler(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Dueler";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.GOLD_HELMET);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[0];
    }
}
