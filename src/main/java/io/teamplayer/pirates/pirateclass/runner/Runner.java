package io.teamplayer.pirates.pirateclass.runner;

import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.weapon.Dagger;
import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Runner extends PirateClass {

    public Runner(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Runner";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.LEATHER_HELMET).setArmorColor(153,153,153);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Dagger()};
    }

    @Override
    public float getDefaultSpeed() {
        return 3f;
    }
}
