package io.teamplayer.pirates.pirateclass.swashbuckler;

import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.weapon.Cutlass;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class Swashbuckler extends PirateClass {

    public Swashbuckler(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Swashbuckler";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.LEATHER_HELMET);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Cutlass()};
    }
}
