package io.teamplayer.pirates.pirateclass.buff;

import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.weapon.Dagger;
import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Buff extends PirateClass {

    public Buff(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Buff";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.LEATHER_HELMET).setArmorColor(242,127,165);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Dagger()};
    }
}
