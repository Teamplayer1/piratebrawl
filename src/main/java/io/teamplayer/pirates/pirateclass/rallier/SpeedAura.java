package io.teamplayer.pirates.pirateclass.rallier;

import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.Aura;
import org.bukkit.Location;

public class SpeedAura extends Aura {

    private final PiratePlayer player;

    SpeedAura(double auraDistance, PiratePlayer player) {
        super(player.getLocation(), auraDistance);
        this.player = player;
    }

    @Override
    protected void enterAura(PiratePlayer player) {
        player.setWalkSpeed(player.getWalkSpeed() * 1.5F);
    }

    @Override
    protected void leftAura(PiratePlayer player) {
        player.setWalkSpeed(player.getPirateClass().getDefaultSpeed());
    }

    @Override
    protected Location getLocation() {
        return player.getLocation();
    }
}
