package io.teamplayer.pirates.pirateclass.rallier;

import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.weapon.Dagger;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Rallier extends PirateClass {

    public Rallier(PiratePlayer player, PirateClassType type) {
        super(player, type);

        new SpeedAura(3.4,player);
    }

    @Override
    public String getName() {
        return "Rallier";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.WOOL,1,player.getTeam().dataColor);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Dagger()};
    }

    @Override
    public LibItem getIcon() {
        return new LibItem(Material.WOOL);
    }
}
