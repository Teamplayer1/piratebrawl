package io.teamplayer.pirates.pirateclass.captain;

import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.weapon.Rapier;
import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Captain extends PirateClass {

    public Captain(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Captain";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.IRON_HELMET);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Rapier()};
    }

    @Override
    public float getDefaultSpeed() {
        return 1.5f;
    }
}
