package io.teamplayer.pirates.pirateclass.marksman;

import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Marksman extends PirateClass {

    public Marksman(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Marksman";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.LEATHER_HELMET).setArmorColor(Color.fromRGB(76,76,76));
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[0];
    }
}
