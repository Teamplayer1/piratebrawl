package io.teamplayer.pirates.pirateclass.blunderer;

import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Blunderer extends PirateClass {

    public Blunderer(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Blunderer";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.CHAINMAIL_HELMET);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[0];
    }
}
