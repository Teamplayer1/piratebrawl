package io.teamplayer.pirates.pirateclass;

import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Optional;

public abstract class PirateClass {

    public static final ChatColor ITEM_NAME_COLOR = ChatColor.WHITE;

    protected final PiratePlayer player;
    protected final PirateClassType type;

    public PirateClass(PiratePlayer player, PirateClassType type) {
        this.player = player;
        this.type = type;
    }

    /**
     * Get the name of the class
     * @return the name of the class
     */
    public abstract String getName();

    /**
     * Get the hat that goes on the head of all players with the class
     * @return the hat for the class
     */
    public abstract LibItem getHat();

    /**
     * Get what should be in the inventories of people with the class
     * @return inventory contents
     */
    public abstract ItemStack[] getLoadout();

    /**
     * Get active potion effects that a player with the class will have
     * @return potion effects for players with the class
     */
    public Optional<PotionEffect> getActiveEffect(){return Optional.empty();}

    /**
     * Get the default walk speed of a player with the class will have
     * @return the default walk speed for players with the class
     */
    public float getDefaultSpeed() {return 0.2f;}

    /**
     * Get the icon that will represent the class in a selector and in the shop
     * @return get the store/shop icon
     */
    public LibItem getIcon() {
        return getHat();
    }

    /**
     * The amount of health a player gets every two ticks while regening
     * @return player regen rate
     */
    public float getRegenRate() {
        return .2F;
    }

    /**
     * How long after a player takes damage in seconds until they start regenning again
     * @return player regen cooldown
     */
    public int getRegenCooldown() {
        return 7;
    }

    /**
     * Get the default amount of max health for this class
     * @return default max health
     */
    public double getMaxHealth() {
        return 20D;
    }

}
