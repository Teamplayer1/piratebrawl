package io.teamplayer.pirates.pirateclass.cook;

import io.teamplayer.pirates.pirateclass.PirateClassType;
import io.teamplayer.pirates.weapon.Cutlass;
import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Cook extends PirateClass {

    public Cook(PiratePlayer player, PirateClassType type) {
        super(player, type);
    }

    @Override
    public String getName() {
        return "Cook";
    }

    @Override
    public LibItem getHat() {
        return new LibItem(Material.LEATHER_HELMET).setArmorColor(216,127,51);
    }

    @Override
    public ItemStack[] getLoadout() {
        return new ItemStack[]{new Cutlass()};
    }
}
