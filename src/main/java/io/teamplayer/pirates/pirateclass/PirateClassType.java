package io.teamplayer.pirates.pirateclass;

import io.teamplayer.pirates.pirateclass.blunderer.Blunderer;
import io.teamplayer.pirates.pirateclass.buff.Buff;
import io.teamplayer.pirates.pirateclass.captain.Captain;
import io.teamplayer.pirates.pirateclass.cook.Cook;
import io.teamplayer.pirates.pirateclass.doctor.Doctor;
import io.teamplayer.pirates.pirateclass.dueler.Dueler;
import io.teamplayer.pirates.pirateclass.marksman.Marksman;
import io.teamplayer.pirates.pirateclass.rallier.Rallier;
import io.teamplayer.pirates.pirateclass.runner.Runner;
import io.teamplayer.pirates.pirateclass.swashbuckler.Swashbuckler;
import io.teamplayer.pirates.player.PiratePlayer;

import java.lang.reflect.InvocationTargetException;

public enum PirateClassType {
    BLUNDERER(Blunderer.class),
    BUFF(Buff.class),
    CAPTAIN(Captain.class),
    COOK(Cook.class),
    DOCTOR(Doctor.class),
    DUELER(Dueler.class),
    MARKSMAN(Marksman.class),
    RALLIER(Rallier.class),
    RUNNER(Runner.class),
    SWASHBUCKLER(Swashbuckler.class);

    private final Class<? extends PirateClass> pirateClassClass;

    PirateClassType(Class<? extends PirateClass> pirateClassClass) {
        this.pirateClassClass = pirateClassClass;
    }

    public PirateClass build(PiratePlayer player) {
        try {
            return pirateClassClass.getConstructor(PiratePlayer.class, PirateClassType.class).newInstance(player, this);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}
