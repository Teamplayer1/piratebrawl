package io.teamplayer.pirates;

import io.teamplayer.pirates.command.VoteCommand;
import io.teamplayer.pirates.listener.GamePlayerCreator;
import io.teamplayer.pirates.listener.StartCountdownListener;
import io.teamplayer.pirates.util.MiscUtil;
import io.teamplayer.pirates.util.entity.CustomEntityType;
import io.teamplayer.lib.LibCore;
import io.teamplayer.lib.api.GameInstance;
import io.teamplayer.lib.api.GameType;
import io.teamplayer.lib.api.gamelistener.GameStateListenerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class PiratesMain extends JavaPlugin implements GameInstance {

    private static PiratesMain instance;
    private RedissonClient redisson;
    private static GameManager manager;

    private static final String REDIS_URL = System.getenv().get("REDIS_URL");
    private static final String REDIS_PASSWORD = System.getenv().get("REDIS_PASS");

    @Override
    public void onEnable() {
        instance = this;

        Config config = new Config();
        config.useSingleServer()
                .setAddress(REDIS_URL)
                .setPassword(REDIS_PASSWORD)
                .setDatabase(1)
                .setDnsMonitoring(true);

        redisson = Redisson.create(config);

        LibCore.registerGameInstance(this);

        manager = new GameManager();

        LibCore.getInstance().registerCommands(
                new VoteCommand(this)
        );

        /* This isn't the only place where Listeners are registered, if you want to find all of the Listeners that are
         registered, look for the usages of the LibCore.registerEvents() method */
        LibCore.getInstance().registerEvents(
                new GamePlayerCreator()
        );
        GameStateListenerManager.registerListener(new StartCountdownListener());

        CustomEntityType.registerEntities();
    }

    /**
     * For any error that requires a server shutdown
     *
     * @param errorNum
     */
    public static void fatalError(int errorNum) {
        /* ERROR LIST:
        0 - Error when creating the directory that holds the map
        1 - No maps are available in the map directory
        2 - Config file couldn't be found in the directory
        3 - The loadMap() method was called with a map name that isn't available
        4 - IOException when coping over the map from the master directory to the local one
        5 - Wrong amount of the map signs were found during quick check
        */

        Bukkit.broadcastMessage(ChatColor.RED + "Something went wrong!. Sending everyone back to the lobby! Error: " +
                errorNum);
        Bukkit.getLogger().severe("Error occurred that required server shutdown. Error:" + errorNum);
        MiscUtil.sendOut();
        Bukkit.shutdown();
    }

    public static PiratesMain getInstance() {
        return instance;
    }

    public RedissonClient getRedisson() {
        return redisson;
    }

    public static GameManager getManager() {
        return manager;
    }

    @Override
    public short getDefaultMaxPlayerCount() {
        return 24;
    }

    @Override
    public short getDefaultReservedSlots() {
        return 4;
    }

    @Override
    public GameType getGameType() {
        return GameType.PIRATES;
    }

    @Override
    public String getMapName() {
        return null;
    }

    @Override
    public short getLobbyTime() {
        return StartCountdownListener.getInstance().timeLeft();
    }
}


