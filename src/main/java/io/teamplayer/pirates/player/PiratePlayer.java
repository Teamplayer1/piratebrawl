package io.teamplayer.pirates.player;

import io.teamplayer.pirates.Constants;
import io.teamplayer.pirates.pirateclass.PirateClass;
import io.teamplayer.pirates.team.Team;
import io.teamplayer.lib.api.Rank;
import io.teamplayer.lib.api.game.GamePlayer;
import io.teamplayer.lib.api.game.GamePlayerState;
import io.teamplayer.lib.servers.player.PreLoginData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Optional;

public class PiratePlayer extends GamePlayer {

    /**
     * If the player has a captured treasure this will show what team that treasure is from
     */
    private Team capturedTreasure;

    /**
     * If the player is capturing a treasure this will show what team that treasure is from
     */
    private Team capturingTreasure;

    /**
     * If the player is currently capturing a treasure.
     */
    private boolean isCapturing;

    /**
     * If the player is currently returning their treasure
     */
    private boolean isReturning;
    private PirateClass pirateClass;

    public PiratePlayer(Player player, PreLoginData data, GamePlayerState state) {
        super(player, data, state);
    }

    /**
     * Get the {@link PiratePlayer} object that belongs to the player based off of the Bukkit player. If this is
     * empty that means the player's data couldn't be retrieved and the player has already been kicked as a result
     * <p>
     * NOTE: You only need to use this on login, every other place where you encounter a {@link Player}, you should be
     * able to just cast it to a {@link PiratePlayer}
     *
     * @param player the player you want the {@link PiratePlayer} object off
     * @return the {@link GamePlayer} that belongs to this player
     */
    public static Optional<PiratePlayer> getPiratePlayer(Player player) {
        return Bukkit.getOnlinePlayers().stream()
                .filter(p -> p instanceof PiratePlayer)
                .map(p -> ((PiratePlayer) p))
                .filter(p -> p.getUniqueId().equals(player.getUniqueId()))
                .findFirst();
    }

    /**
     * Get the team this player is on
     *
     * @return the team this player is on
     */
    public Team getTeam() {
        return Team.getTeamOf(this);
    }

    /**
     * Heal the player by the specified amount of health points by using a very fast regen potion
     *
     * @param heal amount of points to heal the player
     */
    public void heal(double heal) {
        //It looks a little nicer than just setting health because it makes youe hearts dance a little. Tested to be
        // accurate at all healths under 20
        int newHealth = (int) Math.round(pirateClass.getMaxHealth() / 20 * heal);
        addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, newHealth, 5));
    }

    /**
     * Checks if the specified player is the same as this player
     *
     * @param player player to check if equal
     * @return whether or not the players are the same
     */
    public boolean equals(PiratePlayer player) {
        return player.getUuid().equals(getUuid());
    }

    /**
     * Get how many votes this players votes count for when map voting
     *
     * @return the player's voting power
     */
    public byte getVotePower() {
        return (byte) (hasPermission(Rank.ELITE) ? 2 : 1);
    }

    /**
     * Send a message to the player with pirate prefix before the message
     *
     * @param message the message  to send to the player
     */
    public void sendPrefixedMessage(String message) {
        sendMessage(Constants.PREFIX + Constants.PRI + message);
    }

    /**
     * Check to see if the player is alive and able to be interacted with
     *
     * @return whether or not the player is alive
     */
    public boolean isAlive() {
        return getState() == PirateGamePlayerState.ALIVE;
    }

    /**
     * Turns a player into a spectator
     */
    public void makeSpectator() {
        setState(PirateGamePlayerState.SPECTATOR);

    }

    /**
     * Returns the team of the treasure that the player has currently captured, if there is one
     *
     * @return an optional of the team that owns the treasure
     */
    public Optional<Team> getCapturedTreasure() {
        return Optional.of(capturedTreasure);
    }

    /**
     * Get whether or not the player has a treasure on them
     *
     * @return if the player has a treasure
     */
    public boolean hasTreasure() {
        return capturedTreasure != null;
    }

    /**
     * Give the player the treasure of the specified team
     *
     * @param team the team of the treasure being captured
     */
    public void captureTreasure(Team team) {
        capturedTreasure = team;
    }

    /**
     * Drops the treasure the player has captured on the ground, if they have one
     */
    public void dropTreasure() {
        if (!hasTreasure()) return;

        capturedTreasure = null;
    }

    /**
     * Get whether or not the player is capturing or returning a treasure
     *
     * @return if the player is capturing or returning a treasure
     */
    public boolean isInteracting() {
        return isCapturing || isReturning;
    }

    public Team getCapturingTreasure() {
        return capturingTreasure;
    }

    public PiratePlayer setCapturingTreasure(Team capturingTreasure) {
        this.capturingTreasure = capturingTreasure;
        return this;
    }

    public boolean isCapturing() {
        return isCapturing;
    }

    public PiratePlayer setCapturing(boolean capturing) {
        isCapturing = capturing;
        return this;
    }

    public boolean isReturning() {
        return isReturning;
    }

    public PiratePlayer setReturning(boolean returning) {
        isReturning = returning;
        return this;
    }

    public PirateClass getPirateClass() {
        return pirateClass;
    }
}
