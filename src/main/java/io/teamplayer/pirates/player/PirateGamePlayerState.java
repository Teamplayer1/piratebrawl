package io.teamplayer.pirates.player;

import io.teamplayer.lib.api.game.GamePlayerState;

public enum PirateGamePlayerState implements GamePlayerState {
    ALIVE,
    DEAD,
    SPECTATOR;

    PirateGamePlayerState() {

    }

    public boolean isPlaying() {
        return this != SPECTATOR;
    }

    public boolean isSpectating() {
        return this == SPECTATOR;
    }
}
