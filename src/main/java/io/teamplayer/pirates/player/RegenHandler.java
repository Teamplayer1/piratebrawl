package io.teamplayer.pirates.player;

import io.teamplayer.pirates.PiratesMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class RegenHandler implements Listener {

    private final PiratePlayer player;

    //Repletion rates are found in PirateClasses
    private float repletionRate;

    private long lastDamage = 0;
    private double queuedRegen = 0;

    public RegenHandler(PiratePlayer player) {
        this.player = player;

        Bukkit.getPluginManager().registerEvents(this, PiratesMain.getInstance());

        repletionRate = player.getPirateClass().getRegenRate();

        new BukkitRunnable()   {
            @Override
            public void run() {
                if (player.isDead()) return;

                if (!Bukkit.getOnlinePlayers().contains(player)) {
                    cancel();
                }

                if (lastDamage < System.currentTimeMillis() - player.getPirateClass().getRegenCooldown() * 1000) {

                    queuedRegen += repletionRate;

                    if (queuedRegen >= 1) {
                        queuedRegen--;
                        if (player.getHealth() != player.getMaxHealth()) {
                            player.heal(1);
                        }
                    }
                }


            }
        }.runTaskTimer(PiratesMain.getInstance(), 5, 5);
    }

    /**
     * Acts as if the player was just damaged and resets the duration on their regen cooldown
     */
    public void reset() {
        lastDamage = System.currentTimeMillis();
        queuedRegen = 0;
    }

    @EventHandler
    public void cancelNormalHealing(EntityRegainHealthEvent event) {
        if (event.getRegainReason().equals(EntityRegainHealthEvent.RegainReason.SATIATED)) {
            event.setCancelled(true);
        }
    }

    @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void cancelHeal(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && ((Player) event.getEntity()).equals(player)) {
            reset();
        }

    }
}
