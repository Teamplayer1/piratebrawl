package io.teamplayer.pirates;

import io.teamplayer.lib.LibCore;
import org.bukkit.ChatColor;
import org.bukkit.Material;

public final class Constants {

    private Constants() {
        //no instance
    }
    
    public static final String NAME = "Pirates";

    public static final ChatColor PRI = ChatColor.YELLOW;
    public static final ChatColor SEC = ChatColor.WHITE;
    public static final ChatColor TET = ChatColor.RED;

    public static final ChatColor PREFIX_COLOR = ChatColor.RED;
    public static final String PREFIX = PREFIX_COLOR + "PR" + ChatColor.GRAY + LibCore.BULLET_POINT + " ";

    /**
     * The player count at which the starting countdown will start
     */
    public static final short START_COUNTDOWN_COUNT = 16;
    /**
     * The minimum player count that can be in the lobby that the player count can reach before stopping the countdown
     */
    public static final short MIN_PLAYER_COUNT = 12;

    public static final short COUNTDOWN_TIME = 60;

    public static final char GONE_ICON = '◇';
    public static final char THERE_ICON = '◆';
    public static final char STOLEN_ICON = '◈';

    public static final Material[] TREASURE_MATERIALS = new Material[]{Material.IRON_BLOCK, Material.GOLD_BLOCK,
            Material
            .DIAMOND_BLOCK};
}
