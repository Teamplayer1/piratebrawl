package io.teamplayer.pirates.util;

import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.LibRunnable;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;
import java.util.HashSet;

public abstract class Aura {
    private static final long CHECK_FREQUENCY = 5;

    protected final Location location;
    protected final double auraDistance;

    private final double lingerDistance;
    private final BukkitTask task;

    private final Collection<PiratePlayer> inAura = new HashSet<>(6);

    public Aura(Location location, double auraDistance) {
        this.location = location;
        this.auraDistance = Math.pow(auraDistance, 2);
        lingerDistance = Math.pow((auraDistance / 4) + auraDistance, 2);


        task = ((LibRunnable) () -> {
            for (PiratePlayer player : MiscUtil.getPlayers()) {

                double distance = player.getLocation().distanceSquared(getLocation());

                if (inAura.contains(player)) {
                    //Check to see if they left the aura
                    if (distance > lingerDistance) {
                        inAura.remove(player);
                        leftAura(player);
                    }

                } else {
                    //Check to see if they should be in the aura
                    if (distance < auraDistance) {
                        inAura.add(player);
                        leftAura(player);
                    }
                }

            }
        }).runTaskTimer(0, CHECK_FREQUENCY);

    }

    protected abstract void enterAura(PiratePlayer player);

    protected abstract void leftAura(PiratePlayer player);

    protected Location getLocation() {
        return location;
    }
}
