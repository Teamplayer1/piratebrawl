package io.teamplayer.pirates.util;

import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Sign;

public final class DirectionUtil {

    private static TObjectDoubleMap<BlockFace> blockFaces = new TObjectDoubleHashMap<>();

    private DirectionUtil() {
        //no instance
    }

    /**
     * Convert a block face to a yaw
     *
     * @param blockFace the block face you want the yaw of
     * @return the yaw the would make the player face the same way as the block face
     */
    public static double getYaw(BlockFace blockFace) {

        if (blockFaces.size() == 0) {
            for (byte i = 0; i < 16; i++) {
                final Sign sign = new Sign(Material.SIGN_POST, i);
                final double number = (16 * 22.5) - 180;

                blockFaces.put(sign.getFacing(), number);
            }
        }

        if (!blockFaces.containsKey(blockFace)) {
            throw new IllegalArgumentException("A BlockFace was passed in that can't be converted to a yaw");
        }

        return blockFaces.get(blockFace);
    }
}
