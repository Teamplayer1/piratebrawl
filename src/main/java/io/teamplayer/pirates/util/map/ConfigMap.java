package io.teamplayer.pirates.util.map;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Sign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ConfigMap {

    /*
        Get new instances of config map from a MapHandler instance
     */
    private static final List<String> listeningNames = new ArrayList<>();

    private final String name;
    private final boolean gameMap;
    private final boolean properlyConfigured;
    private final ImmutableMultimap<ConfigName, ConfigSign> signMap;

    ConfigMap(String name, World world, boolean gameMap) {
        this.name = name;
        this.gameMap = gameMap;

        //TODO have plugin load in all the signs from the map

    }

    private static List<Sign> getSignsInChunk(Chunk chunk) {
        return Arrays.stream(chunk.getTileEntities())
                .filter(b -> b.getType().equals(Material.SIGN)
                        || b.getType().equals(Material.SIGN_POST))
                .map(b -> ((Sign) b))
                .collect(Collectors.toList());
    }

    public Collection<ConfigSign> getSigns(ConfigName matching) {
        return signMap.get(matching);
    }

    public ConfigSign getSign(ConfigName matching) {
        return Iterables.get(getSigns(matching), 0);
    }

    public String getName() {
        return name;
    }

    public boolean isGameMap() {
        return gameMap;
    }

    public boolean isProperlyConfigured() {
        return properlyConfigured;
    }

    public ImmutableMultimap<ConfigName, ConfigSign> getSignMap() {
        return signMap;
    }
}
