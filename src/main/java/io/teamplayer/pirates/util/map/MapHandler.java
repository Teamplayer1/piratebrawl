package io.teamplayer.pirates.util.map;

import com.google.common.collect.ImmutableList;
import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.util.FileUtil;
import io.teamplayer.lib.api.GameInstance;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.YamlConfiguration;
import org.redisson.api.RMapCache;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static io.teamplayer.pirates.PiratesMain.fatalError;

public class MapHandler {

    private static final String mapPrefix = "Map-";
    private static final File masterFolder = new File(""); //TODO put master folder path
    private static final Pattern mapsPattern = Pattern.compile(mapPrefix + ".*");

    private final RMapCache<String, ConfigMap> mapCache;
    private final File allMapsDir;
    private final File mapDir = new File(Bukkit.getWorldContainer(), "map");
    private final Collection<String> availableMaps = Collections.synchronizedSet(new HashSet<>());

    private ImmutableList<String> cachedAvailableMaps = null;

    /**
     * The map map of the current game map.
     * NOTE: Do not use this getter if the map isn't loaded yet or it will shutdown the server
     *
     * @return The map for the current game map.
     */
    private YamlConfiguration config;

    /**
     * The bukkit world for the map that is currently loaded
     *
     * @return The world that is currently loaded
     */
    private World world;

    public MapHandler(GameInstance game) {
        allMapsDir = new File(masterFolder, game.getGameType().getFullname().toLowerCase() + "/maps");

        updateAvailableMaps();

        boolean completed = false;

        mapCache = PiratesMain.getInstance().getRedisson().getMapCache("maps:" + game.getGameType().getFullname()
                .toLowerCase());

        try {
            completed = mapDir.mkdir();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        if (!completed) {
            fatalError(0);
        }

    }

    /**
     * Load in a map from the master directory with the specified name and return the {@link World} for that loaded
     * in map
     * <p>
     * NOTE: Will return empty if there is any issues when loading the map
     *
     * @param mapName name of the map you want load in
     * @return the {@link World} for the loaded map
     */
    public Optional<World> loadMap(final String mapName) {
        if (!availableMaps.contains(mapName)) {
            fatalError(3);
            return Optional.empty();
        }

        File map = new File(mapDir, mapPrefix + mapName);

        try {
            FileUtil.copyDirectory(map, mapDir);
        } catch (IOException e) {
            e.printStackTrace();
            fatalError(4);
            return Optional.empty();
        }

        Bukkit.getLogger().info("Started loading in game map...");
        Bukkit.createWorld(WorldCreator.name(mapDir.getName()));
        Bukkit.getLogger().info("Finished loading in game map.");

        World world = Bukkit.getWorld(mapDir.getName());
        world.setGameRuleValue("doDayLightCycle", "false");
        world.setGameRuleValue("doMobSpawning", "false");
        world.setAutoSave(false);

        this.world = world;
        config();

        return Optional.of(world);
    }

    /**
     * The maps that are available to be copied in
     *
     * @return an immutable list of the available maps
     */
    public ImmutableList<String> getAvailableMaps() {
        if (cachedAvailableMaps == null) {
            ImmutableList.Builder<String> builder = new ImmutableList.Builder<>();

            synchronized (availableMaps) {
                builder.addAll(availableMaps);
                cachedAvailableMaps = builder.build();
            }
        }

        return cachedAvailableMaps;
    }

    private void updateAvailableMaps() {
        synchronized (availableMaps) {
            availableMaps.clear();
            cachedAvailableMaps = null;

            availableMaps.addAll(
                    Arrays.stream(allMapsDir.listFiles((file, name) -> name.matches(mapsPattern.pattern())))
                            .map(File::getName)
                            .map(s -> s.replaceAll(mapPrefix, ""))
                            .collect(Collectors.toList())
            );
        }

        if (availableMaps.size() == 0) fatalError(1);
    }

    private void config() {
        File file = new File(mapDir, "map.yml");

        if (file.exists()) {
            config = YamlConfiguration.loadConfiguration(file);
        } else {
            fatalError(2);
            config = null;
        }
    }

    /**
     * Gets a {@link ConfigMap} associated with a map name. Either gets a cached version of it from redis, or
     * generates a new one and stores it in redis, then returns that. Method may block
     *
     * @param name    name of the map
     * @param world   the bukkit world for the map
     * @param gameMap whether or not the map is a game map
     * @return the config map for the map
     */
    public ConfigMap getMap(String name, World world, boolean gameMap) {
        if (!mapCache.containsKey(name)) {
            mapCache.put(name, new ConfigMap(name, world, gameMap), 3, TimeUnit.HOURS);
        }

        return mapCache.get(name);
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public World getWorld() {
        return world;
    }
}
