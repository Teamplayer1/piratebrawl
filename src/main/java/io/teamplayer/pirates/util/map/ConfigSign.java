package io.teamplayer.pirates.util.map;

import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;

import java.util.Arrays;

public class ConfigSign {
    //For some reason the field default for making fields public isn't working. Incase you were wondering why i wasn't
    // using it

    public final String worldName;
    private final String[] signContent;

    public final int x, y, z;

    /**
     * The the direction the sign is facing, whether it be on the ground of a wall
     */
    private final byte pos;

    /**
     * True if the original sign was a sign post
     */
    public final boolean isPost;

    ConfigSign(Sign sign, String worldName) {
        this.worldName = worldName;
        signContent = sign.getLines();

        x = sign.getX();
        y = sign.getY();
        z = sign.getZ();

        pos = sign.getRawData();
        isPost = sign.getType() == Material.SIGN_POST;
    }

    private String getData(byte index) {
        return signContent[index];
    }

    public String getString(byte index) {
        return getData(index);
    }

    public boolean getBoolean(byte index) {
        return Boolean.valueOf(getString(index));
    }

    public byte getByte(byte index) {
        return Byte.valueOf(getString(index));
    }

    public float getFloat(byte index) {
        return Float.valueOf(getString(index));
    }

    public int getInt(byte index) {
        return Integer.valueOf(getString(index));
    }

    public double getDouble(byte index) {
        return Double.valueOf(getString(index));
    }

    public BlockFace getFacing() {
        return new org.bukkit.material.Sign(isPost ? Material.SIGN_POST : Material.WALL_SIGN, pos).getFacing();
    }

    public org.bukkit.material.Sign getSignData() {
        return new org.bukkit.material.Sign(isPost ? Material.SIGN_POST : Material.WALL_SIGN, pos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigSign that = (ConfigSign) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        if (z != that.z) return false;
        if (pos != that.pos) return false;
        if (isPost != that.isPost) return false;
        if (!worldName.equals(that.worldName)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(signContent, that.signContent);
    }

    @Override
    public int hashCode() {
        int result = worldName.hashCode();
        result = 31 * result + Arrays.hashCode(signContent);
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + z;
        result = 31 * result + (int) pos;
        result = 31 * result + (isPost ? 1 : 0);
        return result;
    }
}
