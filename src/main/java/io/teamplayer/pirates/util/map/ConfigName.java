package io.teamplayer.pirates.util.map;

public enum ConfigName {
    BANNER("banner", true),
    SPAWN("spawn", true, (byte) 4),
    FIREWORK("firework", true),
    COLORED("colored", true),
    CHEST("chest", true, (byte) 4),
    CRYSTAL("crystal", false, (byte) 1),
    BARRIER("barrier", false, (byte) 4),
    START_HOLO("starting", true, (byte) 4);

    private final String name;
    private final byte requiredAmount;
    private final boolean teamSpecific;

    ConfigName(String name, boolean teamSpecific) {
        this(name, teamSpecific, (byte) -1);
    }

    ConfigName(String name, boolean teamSpecific, byte requiredAmount) {
        this.name = name;
        this.requiredAmount = requiredAmount;
        this.teamSpecific = teamSpecific;
    }

    public static ConfigName getName(String name) {
        for (ConfigName configName : values()) {
            if (configName.toString().equalsIgnoreCase(name)) return configName;
        }
        return null;
    }

    public byte getRequiredAmount() {
        return requiredAmount;
    }

    public boolean isTeamSpecific() {
        return teamSpecific;
    }

    @Override
    public String toString() {
        return "[" + name + ']';
    }
}
