package io.teamplayer.pirates.util.task;

import io.teamplayer.pirates.PiratesMain;
import org.bukkit.Bukkit;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class RepeatingTimer {

    private final Consumer<Integer> runnable;
    private final Runnable finishedRunnable;

    int timesToRepeat;

    ScheduledFuture future;

    /**
     * Creates a repeating task timer that starts after the specified delay and continues to run until canceled or
     * until it's repeated a certain amount of times, then runs another task when it's finished. Consumer gets the
     * amount of times to repeat left
     *
     * @param consumer         runnable task to repeat. consumer accepts the repeating times left
     * @param finishedRunnable runnable that runs once the timer is finished
     * @param timesToRepeat    times the task should be repeated
     * @param delay            delay between the tasks being repeated
     * @param timeUnit         the time unit of the delay between the tasks being repeated
     */
    public RepeatingTimer(Consumer<Integer> consumer, Runnable finishedRunnable, int timesToRepeat, long delay,
                          TimeUnit timeUnit) {
        this.runnable = consumer;
        this.timesToRepeat = timesToRepeat;
        this.finishedRunnable = finishedRunnable;

        future = GameTaskManager.getService().schedule(new CounterWrapper(consumer), delay, timeUnit);
    }

    /**
     * Cancels the timer and doesn't run the finishing task
     */
    public void cancel() {
        future.cancel(false);
    }

    /**
     * Cancels the timer and then runs the finishing task
     */
    public void finish() {
        future.cancel(false);
        finishedRunnable.run();
    }

    private final class CounterWrapper implements Runnable {
        private final Consumer<Integer> runnable;

        CounterWrapper(Consumer<Integer> runnable) {
            this.runnable = runnable;
        }

        @Override
        public void run() {
            Bukkit.getScheduler().runTask(PiratesMain.getInstance(), () -> {
                timesToRepeat--;
                if (timesToRepeat == 0) {
                    future.cancel(false);
                    finishedRunnable.run();
                } else {
                    runnable.accept(timesToRepeat);
                }
            });
        }
    }
}
