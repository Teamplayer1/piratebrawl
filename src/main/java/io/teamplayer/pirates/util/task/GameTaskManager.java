package io.teamplayer.pirates.util.task;

import io.teamplayer.pirates.PiratesMain;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameTaskManager {

    final static ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

    private final List<TaskInfo> queuedRunnabled = new ArrayList<>();

    private boolean gameStarted = false;

    /**
     * Call when the game starts
     */
    public void gameStart() {
        gameStarted = true;

        queuedRunnabled.forEach(taskInfo -> service.schedule(taskInfo.runnable, taskInfo.delay, taskInfo.timeUnit));
    }

    /**
     * If the game has not started this allows you to run a task a certain amount of time after game start, which is
     * when players load into the game map. If the game has already started it will schedule it for the delay to
     * start when the method is called
     *
     * @param task     task to be run at that time
     * @param delay    delay from the start of game/current time to when the task should be run
     * @param timeUnit the timeunits the delay is in
     */
    public void scheduleGameTask(Runnable task, long delay, TimeUnit timeUnit) {
        if (!gameStarted) {
            queuedRunnabled.add(new TaskInfo(new TickWrapper(task), delay, timeUnit));
        } else {
            service.schedule(new TickWrapper(task), delay, timeUnit);
        }
    }

    public static ScheduledExecutorService getService() {
        return service;
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    private final static class TaskInfo {
        final Runnable runnable;
        final long delay;
        final TimeUnit timeUnit;

        public TaskInfo(Runnable runnable, long delay, TimeUnit timeUnit) {
            this.runnable = runnable;
            this.delay = delay;
            this.timeUnit = timeUnit;
        }

        public Runnable getRunnable() {
            return runnable;
        }

        public long getDelay() {
            return delay;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }
    }

    /**
     * Class to wrap a runnable to it always run inside of the MC tick loop
     */
    private final static class TickWrapper implements Runnable {
        final Runnable runnable;

        public TickWrapper(Runnable runnable) {
            this.runnable = runnable;
        }

        @Override
        public void run() {
            Bukkit.getScheduler().runTask(PiratesMain.getInstance(), runnable);
        }
    }
}
