package io.teamplayer.pirates.util.task;

import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.LibRunnable;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

public class TimedPlayerTask {

    /**
     * The task that this player is for
     */
    public final PiratePlayer player;
    /**
     * The chat color that the message should slowly become
     */
    public final ChatColor color;
    /**
     * The message that should be shown that slowly becomes the color
     */
    public final String message;
    /**
     * How long until the task is over in seconds and the runnable is run
     */
    public final short duration;
    /**
     * The task that runs at the end of the duration
     */
    public final Runnable finalTask;

    private int timeLeft;
    private BukkitRunnable currentTask;

    public TimedPlayerTask(PiratePlayer player, ChatColor color, String message, short duration, Runnable finalTask) {
        this.player = player;
        this.color = color;
        this.message = message;
        this.duration = duration;
        this.finalTask = finalTask;
        timeLeft = this.duration * 20;
    }

    {
        currentTask = ((LibRunnable) () -> {
            sendActionBarMessage();

            if (timeLeft == 0) {
                finish();
            }

            timeLeft--;
        }).toBukkit();

        currentTask.runTaskTimer(PiratesMain.getInstance(),0,1);
    }

    private void sendActionBarMessage() {
        double maxTime = duration * 20;
        double timePassed = maxTime - timeLeft;
        short coloredAmount = (short) ((message.length() * timePassed) / maxTime);

        StringBuilder builder = new StringBuilder(message);
        builder.insert(coloredAmount, ChatColor.WHITE + ChatColor.BOLD.toString());
        builder.insert(0, color + ChatColor.BOLD.toString());

        player.sendActionBarMessage(builder.toString());
    }

    /**
     * Cancels the timer and runs the final task
     */
    public void finish() {
        cancel();
        finalTask.run();
    }

    /**
     * Cancels the time
     */
    public void cancel() {
        currentTask.cancel();
    }

    public int getTimeLeft() {
        return timeLeft;
    }
}
