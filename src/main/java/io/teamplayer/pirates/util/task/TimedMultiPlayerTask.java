package io.teamplayer.pirates.util.task;

import io.teamplayer.pirates.team.Team;
import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.LibRunnable;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.HashSet;

public class TimedMultiPlayerTask {
    /*
    This creates a task that gets completed after it's timer reaches zero, the time counts down faster with more
    players added to the task, so that multiple players can contribute to getting on task done
     */

    private static final double PLAYER_ASSISST = 0.5;

    /**
     * The chat color that the message should slowly become
     */
    final ChatColor color;
    /**
     * The message that should be shown that slowly becomes the color
     */
    final String message;
    /**
     * How long until the task is over in seconds and the runnable is run
     */
    final short duration;
    /**
     * The task that runs at the end of the duration
     */
    final Runnable finalTask;

    private final BukkitRunnable currentTask;
    private final Collection<PiratePlayer> players = new HashSet<>(Team.getMaxTeamSize());

    private double timeLeft;
    private double decrementValue = 0;

    public TimedMultiPlayerTask(ChatColor color, String message, short duration, Runnable finalTask) {
        this.color = color;
        this.message = message;
        this.duration = duration;
        this.finalTask = finalTask;

        timeLeft = duration * 20;
    }

    {
        currentTask = ((LibRunnable) () -> {

            if (decrementValue != 0) {
                sendActionBarMessage();

                if (timeLeft == 0) {
                    finish();
                }

                timeLeft-=decrementValue;
            }


        }).toBukkit();

        currentTask.runTaskTimer(PiratesMain.getInstance(), 0, 1);
    }

    private void sendActionBarMessage() {
        final double maxTime = duration * 20;
        final double timePassed = maxTime - timeLeft;
        final short coloredAmount = (short) ((message.length() * timePassed) / maxTime);

        final StringBuilder  builder = new StringBuilder(message);
        builder.insert((int) coloredAmount, ChatColor.WHITE + ChatColor.BOLD.toString());
        builder.insert(0, color + ChatColor.BOLD.toString());


        players.forEach(p -> p.sendActionBarMessage(builder.toString()));
    }


    /**
     * Adds a player to contribute to the task
     *
     * @param player
     */
    public void addPlayer(PiratePlayer player) {
        players.add(player);
        updateDecrement();
    }

    /**
     * Removes a player from contributing to the task
     *
     * @param player the player to remove
     */
    public void removePlayer(PiratePlayer player) {
        players.remove(player);

        if (players.size() > 1) {
            timeLeft = duration * 20;
        }
        updateDecrement();
    }

    private void updateDecrement() {
        if (players.size() > 1) {
            decrementValue = 0;
        } else {
            decrementValue = 1 + (decrementValue * (players.size() - 1));
        }
    }

    /**
     * Cancels the timer and runs the final task
     */
    public void finish() {
        cancel();
        finalTask.run();
    }

    /**
     * Cancels the time
     */
    public void cancel() {
        currentTask.cancel();
    }

    public double getTimeLeft() {
        return timeLeft;
    }

    public double getDecrementValue() {
        return decrementValue;
    }
}
