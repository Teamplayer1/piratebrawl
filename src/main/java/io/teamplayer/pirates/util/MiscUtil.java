package io.teamplayer.pirates.util;

import io.teamplayer.pirates.player.PiratePlayer;
import org.bukkit.Bukkit;

import java.util.Collection;
import java.util.stream.Collectors;

public final class MiscUtil {

    /**
     * Sends all the players out of the game for when the server is shutting down
     */
    public static void sendOut() {
        ((Collection<PiratePlayer>) Bukkit.getOnlinePlayers())
                .forEach(p -> p.sendToServer("Lobby"));
    }

    /**
     * Get the players that are going to/are playing. Basically just a list that excludes spectators
     *
     * @return a collections of non-spectators
     */
    public static Collection<PiratePlayer> getPlayers() {
        return ((Collection<PiratePlayer>) Bukkit.getOnlinePlayers()).stream()
                .filter(p -> p.getState().isPlaying())
                .collect(Collectors.toList());
    }

}
