package io.teamplayer.pirates.util.entity;

import io.teamplayer.lib.api.customentity.RegisterHandler;
import net.minecraft.server.v1_8_R3.EntityBat;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.EntityType;

import java.lang.reflect.Constructor;

 public enum CustomEntityType {
    PEDESTAL("Bat",54, EntityType.BAT, EntityBat.class, CustomEntityPedestal.class);

    private String name;
    private int id;
    private EntityType entityType;
    private Class<? extends EntityInsentient> nmsClass;
    private Class<? extends EntityInsentient> customClass;

     CustomEntityType(String name, int id, EntityType entityType, Class<? extends EntityInsentient> nmsClass,
                             Class<? extends EntityInsentient> customClass) {
        this.name = name;
        this.id = id;
        this.entityType = entityType;
        this.nmsClass = nmsClass;
        this.customClass = customClass;
    }

    public net.minecraft.server.v1_8_R3.Entity spawn(Location location) {
        CraftWorld handle = (CraftWorld) location.getWorld();
        net.minecraft.server.v1_8_R3.Entity entity = null;
        try {
            Constructor<? extends EntityInsentient> constructor = getCustomClass().getConstructor(World.class);
            entity = constructor.newInstance(handle.getHandle());
            handle.getHandle().addEntity(entity);
            entity.setPosition(location.getX(),location.getY(),location.getZ());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public static void registerEntities() {
        for (CustomEntityType customEntity: CustomEntityType.values()) {
            RegisterHandler.registerCustomEntity(customEntity.getName(),customEntity.customClass,customEntity.getID());
        }
    }


    public String getName() {
        return name;
    }

    public int getID() {
        return id;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public Class<? extends EntityInsentient> getNMSClass() {
        return nmsClass;
    }

    public Class<? extends EntityInsentient> getCustomClass() {
        return customClass;
    }
}
