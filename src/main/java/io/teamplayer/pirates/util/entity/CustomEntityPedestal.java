package io.teamplayer.pirates.util.entity;


import net.minecraft.server.v1_8_R3.EntityBat;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Bat;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

 class CustomEntityPedestal extends EntityBat {

    /**
     * The pedestal is placed into the world when it's created at the specified location
     * @param location location for the pedestal to be
     */
     CustomEntityPedestal(Location location) {
        super(((CraftWorld) location.getWorld()).getHandle());

        WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
        setPosition(location.getX(),location.getY(),location.getZ());

        world.addEntity(this);

        Bat bukkit = (Bat) getBukkitEntity();
        bukkit.setMaxHealth(20000D);
        bukkit.setHealth(20000D);
        bukkit.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 0));
    }

    @Override
    protected void E() {
    }

    @Override
    protected String z() {
        return null;
    }

    //Keeps entity from being pushed
    @Override
    public void g(double d0, double d1, double d2) {}
}
