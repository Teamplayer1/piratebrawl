package io.teamplayer.pirates.util.entity;

import io.teamplayer.pirates.Constants;
import io.teamplayer.pirates.team.Team;
import io.teamplayer.lib.api.entities.Hologram;
import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Item;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Treasure {
    //It's not really a custom entity, but whatever

    private final static Map<Team,Treasure> allTreasures = new HashMap<>();
    private final Team team;
    private final Location location;

    private final Item itemEntity;
    private final CustomEntityPedestal pedestal;
    private final Hologram hologram;

    /**
     * Makes a new treasure and spawns it into the world
     *
     * @param location location of the treasure
     * @param team     the team the treasure belongs to
     */
    public Treasure(Location location, Team team) {
        this.team = team;
        this.location = location;

        World world = location.getWorld();

        itemEntity = world.dropItem(location, new LibItem(Constants.TREASURE_MATERIALS[(Constants.TREASURE_MATERIALS.length - team.getTreasures())]));
        pedestal = new CustomEntityPedestal(location);
        hologram = new Hologram(location, (String.valueOf(team.chatColor) +
                ChatColor.BOLD +
                team.name() +
                " teams's treasure").toUpperCase()
        );

        hologram.display();
        pedestal.getBukkitEntity().setPassenger(itemEntity);

        allTreasures.put(team, this);
    }

    /**
     * Removes the treasure from the world
     */
    public void remove() {
        hologram.destroy();
        itemEntity.remove();
        pedestal.getBukkitEntity().remove();
        allTreasures.remove(team);
    }


    public static Optional<Treasure> getTreasure(Team team) {
        return Optional.ofNullable(allTreasures.get(team));
    }

    public Location getLocation() {
        return location;
    }

    public Team getTeam() {
        return team;
    }
}
