package io.teamplayer.pirates.util;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;


public class ImmutableLocation extends Location {

    private ImmutableLocation(Location location) {
        super(location.getWorld(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public static ImmutableLocation build(Location location) {
        return new ImmutableLocation(location);
    }

    public Location nonImmuteable() {
        return new Location(getWorld(), getX(), getY(), getZ(), getYaw(), getPitch());
    }

    @Override
    public void setX(double x) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setY(double y) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setZ(double z) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setYaw(float yaw) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setPitch(float pitch) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location setDirection(Vector vector) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setWorld(World world) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location subtract(Location vec) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location subtract(Vector vec) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location subtract(double x, double y, double z) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location add(Location vec) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location add(Vector vec) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location add(double x, double y, double z) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location multiply(double m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location zero() {
        throw new UnsupportedOperationException();
    }
}
