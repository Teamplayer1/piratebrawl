package io.teamplayer.pirates.util;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;


public final class FileUtil {

    private FileUtil() {
        //no instance
    }
    
    private static void doCopyFile(File srcFile, File destFile) throws IOException {
        if (destFile.exists() && destFile.isDirectory()) {
            throw new IOException("Destination \'" + destFile + "\' exists but is a directory");
        } else {

            try (FileInputStream fis = new FileInputStream(srcFile);
                 FileOutputStream fos = new FileOutputStream(destFile);
                 FileChannel input = fis.getChannel();
                 FileChannel output = fos.getChannel()) {
                long size = input.size();
                long pos = 0L;

                for (long count; pos < size; pos += output.transferFrom(input, pos, count)) {
                    count = ((size - pos) > 31457280L) ? 31457280L : (size - pos);
                }
            }

            if (srcFile.length() == destFile.length()) {
                destFile.setLastModified(srcFile.lastModified());
            } else {
                throw new IOException("Failed to copy full contents from \'" + srcFile + "\' to \'" + destFile + '\'');
            }
        }
    }

    public static void copyDirectory(File srcDir, File destDir) throws IOException {
        if (srcDir == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!srcDir.exists()) {
            throw new FileNotFoundException("Source \'" + srcDir + "\' does not exist");
        } else if (!srcDir.isDirectory()) {
            throw new IOException("Source \'" + srcDir + "\' exists but is not a directory");
        } else if (srcDir.getCanonicalPath().equals(destDir.getCanonicalPath())) {
            throw new IOException("Source \'" + srcDir + "\' and destination \'" + destDir + "\' are the same");
        } else {
            List<String> exclusionList = null;
            if (destDir.getCanonicalPath().startsWith(srcDir.getCanonicalPath())) {
                File[] srcFiles = srcDir.listFiles();
                if ((srcFiles != null) && (srcFiles.length > 0)) {
                    exclusionList = new ArrayList<>(srcFiles.length);
                    for (File srcFile : srcFiles) {
                        File copiedFile = new File(destDir, srcFile.getName());
                        exclusionList.add(copiedFile.getCanonicalPath());
                    }
                }
            }

            doCopyDirectory(srcDir, destDir, exclusionList);
        }
    }

    private static void doCopyDirectory(File srcDir, File destDir, List<String> exclusionList) throws IOException {
        File[] srcFiles = srcDir.listFiles();
        if (srcFiles == null) {
            throw new IOException("Failed to list contents of " + srcDir);
        } else {
            if (destDir.exists()) {
                if (!destDir.isDirectory()) {
                    throw new IOException("Destination \'" + destDir + "\' exists but is not a directory");
                }
            } else if (!destDir.mkdirs() && !destDir.isDirectory()) {
                throw new IOException("Destination \'" + destDir + "\' directory cannot be created");
            }

            if (destDir.canWrite()) {
                for (File srcFile : srcFiles) {
                    File dstFile = new File(destDir, srcFile.getName());
                    if ((exclusionList == null) || !exclusionList.contains(srcFile.getCanonicalPath())) {
                        if (srcFile.isDirectory()) {
                            doCopyDirectory(srcFile, dstFile, exclusionList);
                        } else {
                            doCopyFile(srcFile, dstFile);
                        }
                    }
                }
                destDir.setLastModified(srcDir.lastModified());
            } else {
                throw new IOException("Destination \'" + destDir + "\' cannot be written to");
            }
        }
    }

    public static void deleteDirectory(File directory) throws IOException {
        if (directory.exists()) {
            if (!isSymlink(directory)) {
                if (directory.isDirectory()) {
                    File[] files = directory.listFiles();
                    if (files == null) {
                        throw new IOException("Failed to list contents of " + directory);
                    } else {
                        IOException exception = null;
                        for (File file : files) {
                            try {
                                deleteFile(file);
                            } catch (IOException ex) {
                                exception = ex;
                            }
                        }
                        if (exception != null) {
                            throw exception;
                        }
                    }
                } else {
                    throw new IllegalArgumentException(directory + " is not a directory");
                }
            }

            if (!directory.delete()) {
                throw new IOException("Unable to delete directory " + directory + '.');
            }
        }
    }

    private static void deleteFile(File file) throws IOException {
        if (file.isDirectory()) {
            deleteDirectory(file);
        } else {
            if (!file.delete()) {
                if (!file.exists()) {
                    throw new FileNotFoundException("File does not exist: " + file);
                }
                throw new IOException("Unable to delete file: " + file);
            }
        }
    }

    private static boolean isSymlink(File file) throws IOException {
        if (file == null) {
            throw new NullPointerException("File must not be null");
        } else {
            File fileInCanonicalDir;
            if (file.getParent() == null) {
                fileInCanonicalDir = file;
            } else {
                File canonicalDir = file.getParentFile().getCanonicalFile();
                fileInCanonicalDir = new File(canonicalDir, file.getName());
            }

            return !fileInCanonicalDir.getCanonicalFile().equals(fileInCanonicalDir.getAbsoluteFile());
        }
    }
}
