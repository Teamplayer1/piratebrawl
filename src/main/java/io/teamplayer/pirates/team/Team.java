package io.teamplayer.pirates.team;

import com.google.common.collect.ImmutableList;
import io.teamplayer.pirates.player.PirateGamePlayerState;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.ImmutableLocation;
import io.teamplayer.pirates.util.entity.Treasure;
import io.teamplayer.pirates.util.task.TimedMultiPlayerTask;
import io.teamplayer.pirates.util.task.TimedPlayerTask;
import io.teamplayer.lib.LibCore;
import io.teamplayer.lib.api.GameState;
import io.teamplayer.lib.hostess.LibState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

public enum Team implements Listener {
    RED(Color.fromRGB(153, 51, 51), ChatColor.RED, (byte) 14),
    YELLOW(Color.fromRGB(229, 229, 51), ChatColor.YELLOW, (byte) 4),
    BLUE(Color.fromRGB(51, 76, 178), ChatColor.BLUE, (byte) 11),
    GREEN(Color.fromRGB(102, 127, 51), ChatColor.GREEN, (byte) 13);

    public final Color color;
    public final ChatColor chatColor;
    public final byte dataColor;

    private TeamData data;

    private short spawnIndex = 0;


    //Sorting related vars
    private final static Map<PiratePlayer, Team> playerTeams = new HashMap<>();
    private Stack<PiratePlayer> players = new Stack<>();
    private Queue<PiratePlayer> queue = new LinkedList<>();

    //Treasure related
    private byte treasures = 3;
    private boolean stolen = false;
    private TimedMultiPlayerTask returnTask;
    private Collection<TimedPlayerTask> capturingTasks;

    Team(Color color, ChatColor chatColor, byte dataColor) {
        this.color = color;
        this.chatColor = chatColor;
        this.dataColor = dataColor;

        LibCore.getInstance().registerEvents(this);
    }

    /**
     * Gets the max amount of players a team can have based off of how many people are playing
     *
     * @return max amount of players a team can have
     */
    public static int getMaxTeamSize() {
        return (int) Math.ceil(Bukkit.getOnlinePlayers().stream()
                .map(p -> ((PiratePlayer) p))
                .filter(p -> p.getState() != PirateGamePlayerState.SPECTATOR)
                .count() / values().length);
    }

    /**
     * Get what team the specified player is on
     *
     * @param player player you want the team of
     * @return the team the player is on
     */
    public static Team getTeamOf(PiratePlayer player) {
        return playerTeams.get(player);
    }

    /**
     * Gets whether or not the player is queued to join any team
     *
     * @param player player to check
     * @return whether or not the player is queued to join any team
     */
    public static boolean isQueued(PiratePlayer player) {
        return Arrays.stream(values())
                .map(Team::getQueued)
                .flatMap(Collection::stream)
                .anyMatch(player::equals);
    }

    /**
     * Removes a player from any of the team queues
     *
     * @param player player to remove from queues
     */
    public static void removeFromQueue(PiratePlayer player) {
        if (isQueued(player)) {
            for (Team team : values()) {
                team.unqueuePlayer(player);
            }
        }
    }

    /**
     * Get the team a player would be best on
     *
     * @param player the player who needs a team
     * @return the best team for that player
     */
    private static Team getBestTeam(PiratePlayer player) {
        //TODO come up with a more advance team determining algorithm
        return getLeastPopulatedTeam();
    }


    /**
     * Find the team with the least amount of players
     *
     * @return the team with the least amount of players
     */
    private static Team getLeastPopulatedTeam() {
        Team bestTeam = Team.RED;

        for (Team team : values()) {
            if (team.getSize() < bestTeam.getSize()) {
                bestTeam = team;
            }
        }

        return bestTeam;
    }

    /**
     * Sort all the players that don't have a team onto a team
     */
    public static void finalizeTeams() {
        Bukkit.getOnlinePlayers().stream()
                .map(p -> ((PiratePlayer) p))
                .filter(PiratePlayer::isPlaying)
                .filter(p -> getTeamOf(p) == null)
                .forEach(p -> getBestTeam(p).cleanAdd(p));
    }

    /**
     * Cancels a player's capture task in whatever team it may be in
     *
     * @param player player who's task should be canceled
     */
    public static void cancelPlayerCaptureTask(PiratePlayer player) {
        team:
        for (Team team : values()) {
            for (TimedPlayerTask task : team.capturingTasks) {
                if (task.player.equals(player)) {
                    task.cancel();
                    break team;
                }
            }
        }
    }

    /**
     * Gets the amount of players in the team
     *
     * @return amount of players in the team
     */
    public int getSize() {
        return players.size();
    }

    /**
     * Get the players in the team
     *
     * @return players of the team
     */
    public Collection<PiratePlayer> getPlayers() {
        return new ArrayList<>(players);
    }

    /**
     * Attempts to add a player to the team, and if they can't be added, add them to a queue that will add them
     * when/if there is space in the team. Returns if the player was instantly added
     *
     * @param player The player to be added to the team
     * @return Whether or not the player was instantly added to the team
     */
    public boolean queuePlayer(PiratePlayer player) {
        if (isQueued(player)) {
            Arrays.stream(values())
                    .filter(team -> team.getQueued().contains(player))
                    .forEach(team -> team.unqueuePlayer(player));
        }

        queue.add(player);

        return canJoin();
    }

    /**
     * Removes the player from the queue to join the team
     *
     * @param player the player to remove from the queue
     */
    private void unqueuePlayer(PiratePlayer player) {
        queue.remove(player);
    }

    /**
     * Get the players queued up for joining the team
     *
     * @return players queued up for joining the team
     */
    public List<PiratePlayer> getQueued() {
        return new ArrayList<>(queue);
    }

    /**
     * Moves players into the team if they are in the queue and there is room in the team. Removes players if the
     * amount of max players has gone down
     */
    private void updateRoster() {
        if (getSize() > getMaxTeamSize()) { //Means we need to remove a player from the team

            do {
                PiratePlayer player = players.pop();
                removePlayer(player);

                //There isn't a way to add something to the front of a queue so...
                List<PiratePlayer> tempList = new ArrayList<>(queue);
                queue.clear();

                queue.add(player);
                tempList.forEach(queue::add);

            } while (getSize() > getMaxTeamSize());

        } else if (getSize() < getMaxTeamSize()) { //Means we can add a player to the team if there is any in the queue

            if (!queue.isEmpty()) {

                do {
                    addPlayer(queue.remove());
                } while (getSize() < getMaxTeamSize());
            }
        }
    }

    /**
     * Removes the player from any queues and adds them to a team
     *
     * @param player player to add to team
     */
    private void cleanAdd(PiratePlayer player) {
        removeFromQueue(player);
        addPlayer(player);
    }

    /**
     * Adds the player to the team
     *
     * @param player player to be added to the team
     */
    private void addPlayer(PiratePlayer player) {
        players.push(player);
    }

    /**
     * Removes a player from the team
     *
     * @param player the player to remove from the team
     */
    private void removePlayer(PiratePlayer player) {
        players.remove(player);
        playerTeams.remove(player);
    }

    /**
     * Gets if the a player can be instantly added to a team. Returns true if player can be instantly added to a
     * team, without being queued, and returns false is a player would have to be queued to join the team
     *
     * @return whether or not a player can instantly join
     */
    public boolean canJoin() {
        return getMaxTeamSize() > getSize();
    }

    /*
        Whenever someone leaves remove them from their team and update their roster
     */
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Team team = getTeamOf(((PiratePlayer) event.getPlayer()));

        if (GameState.getGameState() == LibState.Game.IN_LOBBY && team != null) {
            team.removePlayer(((PiratePlayer) event.getPlayer()));
            team.updateRoster();
        }
    }


    /**
     * Sets the team data as well as setting all the team colors for the stuff provided with the data and generating
     * the spawns
     *
     * @param data
     */
    public void setData(TeamData data) {
        this.data = data;

        //Set the colored blocks
        data.getColoredBlocks().forEach(b -> b.setData(dataColor));
    }

    /**
     * Get one of the spawn points for the team. Increments through the different spawn points which each call
     *
     * @return a team spawn point
     */
    public Location getSpawn() {
        ImmutableList<ImmutableLocation> spawns = data.getSpawns();

        spawnIndex++;
        return spawns.get(spawnIndex % spawns.size());
    }

    /**
     * Get whether or not the ship is alive and players can continue to respawn
     *
     * @return whether or not the ship is alive
     */
    public boolean isShipAlive() {
        return treasures > 0;
    }

    /**
     * Steals the treasure from the this team
     *
     * @param stealingPlayer the player that stole the treasure from this team
     */
    public void stealTreasure(PiratePlayer stealingPlayer) {
        stolen = true;

        capturingTasks.forEach(TimedPlayerTask::cancel);
        capturingTasks.clear();

        returnTask = new TimedMultiPlayerTask(chatColor, "Returning Treasure...", (short) 6, () -> {
            returnTreasure();
            Treasure.getTreasure(this).get().remove();
        });
    }

    /**
     * Called when a player successfully captures this teams treasure and bring it back to their ship
     *
     * @param capturedPlayer the player that brought the treasure back to their ship
     */
    public void captureTreasure(PiratePlayer capturedPlayer) {
        stolen = false;
        treasures--;

        if (treasures == 0) eliminateTeam();
    }

    /**
     * Register that a player is starting to capture the treasure
     *
     * @param task the capture treasure task of that player
     */
    public void startCapture(TimedPlayerTask task) {
        capturingTasks.add(task);
    }

    /**
     * Called once treasure is returned. (This method does not return treasure)
     */
    public void returnTreasure() {
        stolen = false;
        returnTask = null;
    }

    /**
     * Kills the ship and all of the players as well as any animations/messages that are sent when a team is eliminated
     */
    public void eliminateTeam() {

    }

    public TeamData getData() {
        return data;
    }

    public byte getTreasures() {
        return treasures;
    }

    public boolean isStolen() {
        return stolen;
    }

    public TimedMultiPlayerTask getReturnTask() {
        return returnTask;
    }
}
