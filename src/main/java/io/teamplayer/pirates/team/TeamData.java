package io.teamplayer.pirates.team;

import com.google.common.collect.ImmutableList;
import io.teamplayer.pirates.util.ImmutableLocation;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;

public class TeamData {
    private final Block chest;
    private final ImmutableList<ImmutableLocation> fireworkSpawns, spawns;
    private final ImmutableList<Block> coloredBlocks;
    private final ImmutableList<Banner> banners;

    public TeamData(Block chest, ImmutableList<ImmutableLocation> fireworkSpawns, ImmutableList<ImmutableLocation> spawns, ImmutableList<Block> coloredBlocks, ImmutableList<Banner> banners) {
        this.chest = chest;
        this.fireworkSpawns = fireworkSpawns;
        this.spawns = spawns;
        this.coloredBlocks = coloredBlocks;
        this.banners = banners;
    }

    public Block getChest() {
        return chest;
    }

    public ImmutableList<ImmutableLocation> getFireworkSpawns() {
        return fireworkSpawns;
    }

    public ImmutableList<ImmutableLocation> getSpawns() {
        return spawns;
    }

    public ImmutableList<Block> getColoredBlocks() {
        return coloredBlocks;
    }

    public ImmutableList<Banner> getBanners() {
        return banners;
    }
}
