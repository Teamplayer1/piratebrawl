package io.teamplayer.pirates.command;

import io.teamplayer.pirates.lobby.VotingHandler;
import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.commands.PlayerCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class VoteCommand extends PlayerCommand {

    public VoteCommand(JavaPlugin plugin) {
        super(plugin, "vote");
    }

    @Override
    public boolean execute(Player player, String[] args) {

        final VotingHandler votingHandler = PiratesMain.getInstance().getManager().getVotingHandler();

        if (args.length != 1) {
            if (votingHandler.isValidMap(args[0])) {
                votingHandler.vote(((PiratePlayer) player), args[0]);
            }
            return true;
        } else {
            return false;
        }
    }
}
