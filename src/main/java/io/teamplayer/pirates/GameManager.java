package io.teamplayer.pirates;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import io.teamplayer.pirates.lobby.VotingHandler;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.team.Team;
import io.teamplayer.pirates.team.TeamData;
import io.teamplayer.pirates.util.BlockFindingUtil;
import io.teamplayer.pirates.util.DirectionUtil;
import io.teamplayer.pirates.util.ImmutableLocation;
import io.teamplayer.pirates.util.map.ConfigMap;
import io.teamplayer.pirates.util.map.ConfigSign;
import io.teamplayer.pirates.util.map.MapHandler;
import io.teamplayer.pirates.util.task.GameTaskManager;
import io.teamplayer.pirates.util.map.ConfigName;
import io.teamplayer.lib.api.GameState;
import io.teamplayer.lib.hostess.LibState;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.material.Banner;
import org.bukkit.material.Sign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GameManager {

    private final VotingHandler votingHandler = new VotingHandler();
    private final GameTaskManager timer = new GameTaskManager();
    private final MapHandler mapHandler = new MapHandler(PiratesMain.getInstance());

    private ConfigMap gameMap;
    private String mapName = "Voting";
    /**
     * Whether or not the map vote has been finalized yet
     */
    private boolean mapFound = false;

    GameManager() {
        GameState.setGameState(LibState.Game.IN_LOBBY);
    }

    /**
     * Call to load everyone into the map and prepare for starting the game
     */
    public void startPregame() {
        GameState.setGameState(LibState.Game.PREGAME);

        //Sort everyone into teams that isn't on a team
        Team.finalizeTeams();
    }

    private void startGame() {
        GameState.setGameState(LibState.Game.IN_PROGRESS);
    }

    /**
     * Loads the map into the server as well as all of the related map info. Also finalized the map voting
     */
    public void loadMap() {
        mapFound = true;

        final String winningMap = votingHandler.getLead();
        final Optional<World> worldOpt = votingHandler.finalizeMap();

        if (!worldOpt.isPresent()) return; //The server will shut down already if there is a error in map loading

        final World world = worldOpt.get();

        gameMap = mapHandler.getMap(winningMap, world, true);

        //Make all the banners
        gameMap.getSigns(ConfigName.BANNER).forEach(data -> {
            final Block block = world.getBlockAt(data.x, data.y, data.z);
            final Material type = data.isPost ? Material.WALL_BANNER : Material.STANDING_BANNER;

            final Banner bannerData = new Banner(type);
            bannerData.setFacingDirection(data.getFacing());

            block.setType(type);
            block.setData(bannerData.getData());

            ((org.bukkit.block.Banner) block.getState()).update(true);
        });

        //Assign the teams their data
        final int amountOfTeams = Team.values().length;
        final ArrayList<Multimap<ConfigName, ConfigSign>> teamSigns = new ArrayList<Multimap<ConfigName, ConfigSign>>(amountOfTeams);

        for (int i = 0; i < amountOfTeams; i++) {
            int finalI = i;

            List<ConfigSign> signs = gameMap.getSignMap().values().stream()
                    .filter(c -> ConfigName.getName(c.getString((byte) 0)).isTeamSpecific())
                    .filter(c -> c.getByte((byte) 1) == finalI)
                    .collect(Collectors.toList());

            final Multimap<ConfigName, ConfigSign> multiMap = ArrayListMultimap.create();

            for (ConfigSign sign : signs) {
                multiMap.put(ConfigName.getName(sign.getString((((byte) 0)))), sign);
            }

            teamSigns.set(i, multiMap);
        }

        final ArrayList<TeamData> teamData = new ArrayList<TeamData>(amountOfTeams);

        class LocationHelper {
            private ImmutableLocation getLoc(ConfigSign sign) {
                return ImmutableLocation.build(new Location(world, sign.x, sign.y, sign.z, (float) DirectionUtil.getYaw(sign
                        .getFacing()), 0));
            }

            private Block getBlock(ConfigSign sign) {
                return world.getBlockAt(getLoc(sign));
            }
        }

        final LocationHelper helper = new LocationHelper();

        for (int i = 0; i < amountOfTeams; i++) {
            final ImmutableLocation defaultSpawn = ImmutableLocation.build(world.getSpawnLocation());
            final Multimap<ConfigName, ConfigSign> multiMap = teamSigns.get(i);

            final ImmutableList.Builder<org.bukkit.block.Banner> banners = new ImmutableList.Builder<org.bukkit.block.Banner>();
            final ImmutableList.Builder<ImmutableLocation> fireworkLocs = new ImmutableList.Builder<ImmutableLocation>();
            final ImmutableList.Builder<Block> coloredBlocks = new ImmutableList.Builder<Block>();
            final ImmutableList.Builder<ImmutableLocation> spawnLocs = new ImmutableList.Builder<ImmutableLocation>();

            final Block chest = helper.getBlock(Iterables.get(multiMap.get(ConfigName.CHEST), 0));

            banners.addAll(teamSigns.get(i).get(ConfigName.BANNER).stream()
                    .map(sign -> ((org.bukkit.block.Banner) helper.getBlock(sign).getState()))
                    .collect(Collectors.toList()));

            fireworkLocs.addAll(teamSigns.get(i).get(ConfigName.FIREWORK).stream()
                    .map(helper::getLoc)
                    .collect(Collectors.toList()));

            final List<ImmutableLocation> spawns = teamSigns.get(i).get(ConfigName.SPAWN).stream()
                    .map(helper::getLoc)
                    .collect(Collectors.toList());

            Collections.shuffle(spawns);

            spawnLocs.addAll(spawns);

            for (ConfigSign sign : teamSigns.get(i).get(ConfigName.COLORED)) {
                final Sign signData = sign.getSignData();

                coloredBlocks.addAll(BlockFindingUtil.findExact(helper.getBlock(sign).getRelative(signData
                        .getAttachedFace())));
            }

            teamData.set(i, new TeamData(chest, fireworkLocs.build(), spawnLocs.build(), coloredBlocks.build(), banners.build()));
        }

        Collections.shuffle(teamData);

        for (int i = 0; i < amountOfTeams; i++) {
            Team.values()[i].setData(teamData.get(i));
        }

        //Spawn the players into the world

        //Players on a team go to their team spawn
        Bukkit.getOnlinePlayers().stream()
                .map(p -> ((PiratePlayer) p))
                .filter(PiratePlayer::isPlaying)
                .forEach(p -> p.teleport(p.getTeam().getSpawn()));

        //Spectators get teleported to the crystal
        Bukkit.getOnlinePlayers().stream()
                .map(p -> ((PiratePlayer) p))
                .filter(PiratePlayer::isSpectating)
                .forEach(p -> p.teleport(helper.getLoc(gameMap.getSign(ConfigName.CRYSTAL))));
    }

    public VotingHandler getVotingHandler() {
        return votingHandler;
    }

    public GameTaskManager getTimer() {
        return timer;
    }

    public MapHandler getMapHandler() {
        return mapHandler;
    }

    public ConfigMap getGameMap() {
        return gameMap;
    }

    public String getMapName() {
        return mapName;
    }

    public boolean isMapFound() {
        return mapFound;
    }
}
