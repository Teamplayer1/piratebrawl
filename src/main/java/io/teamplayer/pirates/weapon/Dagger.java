package io.teamplayer.pirates.weapon;

import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;

import static io.teamplayer.pirates.pirateclass.PirateClass.ITEM_NAME_COLOR;

public class Dagger extends LibItem {
    public Dagger() {
        super(Material.WOOD_SWORD);
        setName(ITEM_NAME_COLOR + "Dagger");
    }
}
