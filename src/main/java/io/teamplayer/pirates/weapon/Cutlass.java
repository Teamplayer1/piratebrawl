package io.teamplayer.pirates.weapon;

import io.teamplayer.lib.api.inventory.LibItem;
import org.bukkit.Material;

import static io.teamplayer.pirates.pirateclass.PirateClass.ITEM_NAME_COLOR;

public class Cutlass extends LibItem {
    public Cutlass() {
        super(Material.STONE_SWORD);
        setName(ITEM_NAME_COLOR + "Cutlass");
    }
}
