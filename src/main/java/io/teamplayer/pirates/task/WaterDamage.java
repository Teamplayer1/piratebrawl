package io.teamplayer.pirates.task;

import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.MiscUtil;
import org.bukkit.Material;

public class WaterDamage implements Runnable {

    /**
     * How often this should run in ticks
     */
    public static final short CHECK_FREQUNCY = 15;

    @Override
    public void run() {
        MiscUtil.getPlayers().stream()
                .filter(this::isInWater)
                .forEach(p -> p.damage(3));
    }

    private boolean isInWater(PiratePlayer player) {
        return player.getLocation().getBlock().getType().equals(Material.WATER);
    }
}
