package io.teamplayer.pirates.task;

import io.teamplayer.pirates.team.Team;
import io.teamplayer.pirates.util.task.TimedPlayerTask;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.MiscUtil;
import io.teamplayer.pirates.util.entity.Treasure;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Optional;

public class CaptureDetection implements Runnable {

    /**
     * How often this should run in ticks
     */
    public static final short CHECK_FREQUNCY = 10;

    @Override
    public void run() {

        final ArrayList<PiratePlayer> nearChest = new ArrayList<PiratePlayer>();

        team:
        for (Team team : Team.values()) {
            final Optional<Treasure> treasureOptional = Treasure.getTreasure(team);

            for (PiratePlayer player : MiscUtil.getPlayers()) {
                if (player.isDead()) continue;

                if (!player.isInteracting()) {
                    //We need to check to see if the player should start returning/capturing a treasure

                    if (team.isStolen()) {


                        if (treasureOptional.isPresent()) {
                            final Treasure treasure = treasureOptional.get();

                            if (player.getTeam() == team) {
                                //Check to return the treasure
                                if (!player.isCapturing() && isNear(player.getLocation(), treasure.getLocation())) {
                                    //Start returning the treasure
                                    team.getReturnTask().addPlayer(player);
                                }
                            } else {
                                //Check to pickup the treasure
                                if (!player.hasTreasure() && isNear(player.getLocation(), treasure.getLocation())) {
                                    //TODO "blank has picked up blank team's treasure"
                                    player.captureTreasure(team);
                                    treasure.remove();
                                }
                            }


                        } else {
                            //If the team treasure is not dropped we should check to see if this teams treasure should be
                            // captured
                            final PiratePlayer thief = MiscUtil.getPlayers().stream() // first see who has the treasure
                                    .filter(PiratePlayer::hasTreasure)
                                    .filter(p -> p.getCapturedTreasure().get() == team)
                                    .findFirst()
                                    .get();

                            if (isNear(thief.getLocation(), thief.getTeam().getData().getChest().getLocation())) {
                                if (!thief.getTeam().isStolen()) {
                                    team.stealTreasure(thief);
                                } else {
                                    //TODO Tell them that they can't capture it yet
                                }
                            }

                            continue team;
                        }

                    } else {
                        //If the treasure is not stolen then we need to check if anyone can start capturing it
                        if (player.getTeam() != team && !player.isInteracting()) {
                            final TimedPlayerTask task = new TimedPlayerTask(player, team.chatColor, "Capturing Treasure...",
                                    (short) 5, () -> {
                                player.captureTreasure(team);
                                team.captureTreasure(player);
                            });

                            team.startCapture(task);
                            player.setCapturingTreasure(team);
                        }
                    }


                } else {
                    //Check to see if they need to stop capturing/returning

                    if (player.isCapturing()) {

                        if (!isNear(player.getLocation(),team.getData().getChest().getLocation())) {
                            Team.cancelPlayerCaptureTask(player);
                            player.setCapturingTreasure(null);
                        }

                    } else if (player.isReturning()) {
                        if (player.getTeam() != team) continue;

                        if (!isNear(player.getLocation(),treasureOptional.get().getLocation())) {
                            team.getReturnTask().removePlayer(player);
                        }
                    }
                }

            }
        }


    }

    private boolean isNear(Location playerLoc, Location treasureLoc) {
        return playerLoc.distanceSquared(treasureLoc) < 1.5;
    }
}
