package io.teamplayer.pirates.listener;

import io.teamplayer.pirates.Constants;
import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.task.RepeatingTimer;
import io.teamplayer.lib.api.GameState;
import io.teamplayer.lib.api.gamelistener.GameStateListener;
import io.teamplayer.lib.hostess.LibState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class StartCountdownListener implements GameStateListener {

    private static StartCountdownListener instance;

    private RepeatingTimer timer = null;
    private short timeLeft = -1;

    {
        instance = this;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        final Optional<PiratePlayer> player = PiratePlayer.getPiratePlayer(event.getPlayer());

        if (getActualPlayerCount() >= Constants.START_COUNTDOWN_COUNT) {
            startTimer();
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        final Optional<PiratePlayer> player = PiratePlayer.getPiratePlayer(event.getPlayer());

        if (getActualPlayerCount() < Constants.MIN_PLAYER_COUNT) {
            stopTimer();
        }

    }

    private short getActualPlayerCount() {
        return (short) Bukkit.getOnlinePlayers().stream()
                .map(p -> ((PiratePlayer) p))
                .filter(PiratePlayer::isPlaying)
                .count();
    }

    private void startTimer() {
        if (timer == null) {
            timer = new RepeatingTimer(time -> {
                for (PiratePlayer player : (Collection<PiratePlayer>) Bukkit.getOnlinePlayers()) {

                    //Send the action bar message
                    player.sendActionBarMessage(Constants.PRI + ChatColor.BOLD.toString() + "Joining map " + ChatColor.GRAY +
                            "» " + Constants.SEC + ChatColor.BOLD.toString() + time + " seconds");

                    //Update lobby time for lobbies
                    GameState.setLobbyTime(time.shortValue());
                    timeLeft = time.shortValue();

                    //Make the sounds at certain intervals
                    if (time % 10 == 0 && time != 10) {
                        player.playSound(player.getLocation(), Sound.CLICK, 1.0F, 1.0F);
                    } else if (time <= 5 || time == 10) {
                        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
                    }

                    //Load the map
                    if (time == 10 && !PiratesMain.getInstance().getManager().isMapFound()) {
                        PiratesMain.getInstance().getManager().loadMap();
                    }
                }
            }, () -> PiratesMain.getInstance().getManager().startPregame(), 60, 1, TimeUnit.MINUTES);
        }
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public short timeLeft() {
        if (timer == null) {
            return -1;
        } else {
            return timeLeft;
        }
    }

    @Override
    public LibState.Game getAssociatedGS() {
        return LibState.Game.IN_LOBBY;
    }

    public static StartCountdownListener getInstance() {
        return instance;
    }
}
