package io.teamplayer.pirates.listener;

import io.teamplayer.pirates.player.PirateGamePlayerState;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.servers.player.PreLoginData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class GamePlayerCreator implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onAsyncLogin(AsyncPlayerPreLoginEvent event) {
        PreLoginData.create(event.getUniqueId());
    }

    @EventHandler(ignoreCancelled = true)
    public void onJoin(PlayerJoinEvent event) {
        new PiratePlayer(event.getPlayer(), PreLoginData.get(event.getPlayer().getUniqueId()), PirateGamePlayerState.ALIVE);
    }
}
