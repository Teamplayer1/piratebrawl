package io.teamplayer.pirates.listener;

import io.teamplayer.pirates.player.PirateGamePlayerState;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.lib.api.TitleBuilder;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DeathCatcher implements Listener {


    private void handleDeath(PiratePlayer player) {

        if (player.getTeam().isShipAlive()) {
            kill(player);
        } else {

        }
    }

    private void kill(PiratePlayer player) {
        player.setState(PirateGamePlayerState.DEAD);
    }

    private void respawn(PiratePlayer player) {
        player.setState(PirateGamePlayerState.ALIVE);
    }

    private void sendRespawningTitle(PiratePlayer player, byte timeLeft) {
        final TitleBuilder title = new TitleBuilder();
        title.fadeIn(0);
        title.fadeOut(10);
        title.stayTime(timeLeft > 1 ? 20 * 3 : 20);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void catchDeath(EntityDamageEvent event) {

        if (event.getEntity() instanceof PiratePlayer) {

            final PiratePlayer player = ((PiratePlayer) event.getEntity());

            if (player.isAlive() && player.getHealth() - event.getDamage() <= 0) {
                event.setCancelled(true); //We don't want them to actually die
                handleDeath(player);
            }

        }
    }
}
