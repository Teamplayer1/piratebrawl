package io.teamplayer.pirates.lobby;

import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.util.map.ConfigMap;
import org.bukkit.Bukkit;

public class LobbyManager {

    private ConfigMap configMap;

    {
        configMap = PiratesMain.getManager().getMapHandler().getMap("lobby", Bukkit.getWorlds().get(0),false);
    }

    public ConfigMap getConfigMap() {
        return configMap;
    }
}
