package io.teamplayer.pirates.lobby;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.teamplayer.pirates.Constants;
import io.teamplayer.pirates.PiratesMain;
import io.teamplayer.pirates.player.PiratePlayer;
import io.teamplayer.pirates.util.map.MapHandler;
import io.teamplayer.lib.LibCore;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class VotingHandler implements Listener {

    private Multimap<String, PiratePlayer> mapVotes = ArrayListMultimap.create();
    private MapHandler mapHandler = new MapHandler(PiratesMain.getInstance());
    /**
     * The maps that are available to be voted for
     *
     * @return the maps that can be voted for
     */
    private String[] mapsForVoting;

    {
        //Set maps for voting to be an array of three random available maps
        List<String> availableMaps = mapHandler.getAvailableMaps();
        Collections.shuffle(availableMaps);
        mapsForVoting = availableMaps.stream()
                .limit(3)
                .toArray(String[]::new);

        //Register as a listener so we can send players a message on login
        LibCore.getInstance().registerEvents(this);
    }

    /**
     * Get the amount of votes a specified map has based off of the vote power of the people who voted for the map
     *
     * @param mapName name of the map
     * @return votes for the map
     */
    public short getMapVotes(String mapName) {
        return (short) mapVotes.get(mapName).stream()
                .mapToInt(PiratePlayer::getVotePower)
                .sum();
    }

    /**
     * Get the map that currently has the most votes
     *
     * @return the highest votes map
     */
    public String getLead() {

        String topMap = "";
        short topVotes = -1;

        for (String map : mapsForVoting) {
            short votes = (short) mapVotes.get(map).stream()
                    .filter(Player::isOnline) //Player's votes of players that left don't count
                    .mapToInt(PiratePlayer::getVotePower)
                    .sum();

            if (votes > topVotes) {
                topVotes = votes;
                topMap = map;
            }
        }

        return topMap;
    }

    /**
     * Gets whether a not a string is one of the names of the maps that is available for voting
     *
     * @param mapName the map name to check
     * @return whether or not that map name is one of the maps available for voting
     */
    public boolean isValidMap(String mapName) {

        for (String map : mapsForVoting) {
            if (map.equalsIgnoreCase(mapName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the map that has the most votes and attempt to load it into server instance folder and returns the bukkit
     * world of that loaded map
     *
     * @return the bukkit world that corresponds to the loaded map
     */
    public Optional<World> finalizeMap() {
        return mapHandler.loadMap(getLead());
    }

    /**
     * Vote for a player for a certain map and send them a message that confirms it
     *
     * @param player the player that is voting
     * @param map    the map the player is voting for
     */
    public void vote(PiratePlayer player, String map) {
        getPlayerVote(player).ifPresent(previousMap -> mapVotes.remove(previousMap, player));

        mapVotes.put(map, player);
        player.sendPrefixedMessage("You voted for " + Constants.SEC + map + Constants.PRI + "!");
    }

    /**
     * Gets the map the player voted for, returns empty if they haven't voted for anything
     *
     * @param player that player who voted
     * @return the map the player voted for
     */
    private Optional<String> getPlayerVote(PiratePlayer player) {
        if (mapVotes.containsValue(player)) {
            for (String map : mapsForVoting) {
                if (mapVotes.containsEntry(map, player)) {
                    return Optional.of(map);
                }
            }
        }

        return Optional.empty();
    }

    @EventHandler
    public void sendVoteMessages(PlayerLoginEvent event) {
        final Optional<PiratePlayer> playerOpt = PiratePlayer.getPiratePlayer(event.getPlayer());
        if (!playerOpt.isPresent()) return;

        final PiratePlayer player = playerOpt.get();

        player.sendPrefixedMessage("Vote for the next map to be played on" + ChatColor.GRAY + "(Click to vote)");
        //Send the messages that allows them to vote easily
        for (String mapName : mapsForVoting) {

            final ComponentBuilder text = new ComponentBuilder(" - ")
                    .color(ChatColor.GRAY.asBungee())
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Vote for")
                            .color(Constants.PRI.asBungee())
                            .append(mapName)
                            .color(Constants.SEC.asBungee())
                            .create()))
                    .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/vote " + mapName))
                    .append(mapName, ComponentBuilder.FormatRetention.EVENTS)
                    .color(Constants.SEC.asBungee());

            player.spigot().sendMessage(text.create());

        }

    }

    public String[] getMapsForVoting() {
        return mapsForVoting;
    }
}
